#include <conio.h>
#include <graphics.h>
#include<stdlib.h>
#pragma comment(lib,"Winmm.lib")
IMAGE img_bk, img_bd1, img_bd2, img_bar_up1, img_bar_up2, img_bar_down1, img_bar_down2;
int bird_x;
int bird_y;
int bar_x;
void startup()  
{
	mciSendString("open D:\\background.mp3 alias bkmusic", NULL, 0, NULL);
	mciSendString("play bkmusic repeat", NULL, 0, NULL);
	initgraph(350, 600);
	loadimage(&img_bk, "D:\\background.jpg");
	loadimage(&img_bd1, "D:\\bird1.jpg");
	loadimage(&img_bd2, "D:\\bird2.jpg");
	loadimage(&img_bar_up1, "D:\\bar_up1.gif");
	loadimage(&img_bar_up2, "D:\\bar_up2.gif");
	loadimage(&img_bar_down1, "D:\\bar_down1.gif");
	loadimage(&img_bar_down2, "D:\\bar_down2.gif");
	bird_x = 50;
	bird_y = 200;
	bar_x = 180;
	BeginBatchDraw();
}
void show() 
{
	putimage(0, 0, &img_bk);
	putimage(bar_x, -300, &img_bar_up1, NOTSRCERASE);
	putimage(bar_x, -300, &img_bar_up2, SRCINVERT);
	putimage(bar_x, 400, &img_bar_down1, NOTSRCERASE);
	putimage(bar_x, 400, &img_bar_down2, SRCINVERT);
	putimage(bird_x, bird_y, &img_bd1, NOTSRCERASE);
	putimage(bird_x, bird_y, &img_bd2, SRCINVERT);
	FlushBatchDraw();
	Sleep(50);
}
void updateWithoutInput()  
{
	if (bird_y < 580)
		bird_y = bird_y + 3;
	if (bar_x < 0)
		bar_x = 180;
	else
		bar_x--;
	if (bird_x == bar_x && (bird_y < 280 || bird_y>390)) {
		exit(0);
	}
}

void updateWithInput()  
{
	char input;
	if (kbhit()) {
		input = getch();
		if (input == ' ' && bird_y > 20) {
			bird_y = bird_y - 30;
			mciSendString("close jpmusic", NULL, 0, NULL);
			mciSendString("open D:\\Jump.mp3 alias jpmusic", NULL, 0, NULL);
			mciSendString("play jpmusic", NULL, 0, NULL);
		}
	}
}
void gameover()
{
	EndBatchDraw();
	getch();
	closegraph();
}
int main()
{
	startup();  
	while (1)  
	{
		
		updateWithoutInput();  
		updateWithInput();     
		show(); 
	}
	gameover();     
	return 0;
}